﻿using Unity.Entities;
using UnityEngine;

public class AddLinkedEntityGroupOnConversion : MonoBehaviour, IConvertGameObjectToEntity
{
    public void Convert(Entity entity,
                        EntityManager dstManager,
                        GameObjectConversionSystem conversionSystem) =>
        conversionSystem.DeclareLinkedEntityGroup(this.gameObject);

}
