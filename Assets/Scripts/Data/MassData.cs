﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct MassData : IComponentData
{
    public int Value;
}
