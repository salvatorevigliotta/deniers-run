﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct DeniersMassData : IComponentData
{
    public float Value;
}
