﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct AllDeniesMassData : IComponentData
{
    public float Value;
}
