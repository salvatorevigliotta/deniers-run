﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct GroundData : IComponentData
{
    public bool IsOnGround;
}
