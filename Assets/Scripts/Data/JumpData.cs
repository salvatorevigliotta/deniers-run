﻿using Unity.Entities;

[GenerateAuthoringComponent]
public struct JumpData : IComponentData
{
    public float JumpSpeed;
    public bool IsJumping;
}
