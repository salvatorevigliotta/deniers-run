﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;

public class PeopleMoveSystem : SystemBase
{
    protected override void OnUpdate()
    {
        var deltaTime = Time.DeltaTime;
        Entities.ForEach((ref Translation translation, in MoveData moveData) => 
        {
            translation.Value.x += 1f * deltaTime * moveData.Value;
        }).Schedule();
    }
}
