﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Transforms;

public class SpawnSystem : SystemBase
{
    protected override void OnUpdate()
    {
        var newX = UnityEngine.Random.Range(40f, 80f);
        var newZ = UnityEngine.Random.Range(-1f, 1f);
        Entities.
            WithStructuralChanges().
            ForEach((Entity entity, in Translation translation) =>
            {
                if (translation.Value.x < -20f)
                {
                    //TODO spawn
                    var newEnity = EntityManager.Instantiate(entity);
                    EntityManager.SetComponentData<Translation>(newEnity, new Translation()
                    {
                        Value = new float3(newX, 0, newZ),
                    });

                    EntityManager.DestroyEntity(entity);
                }
            }).Run();
    }
}
