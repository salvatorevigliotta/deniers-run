﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;
using UnityEngine;

public class JumpSystem : SystemBase
{
    //TODO: Improvement movement with offset
    protected override void OnUpdate()
    {
        var deltaTime = Time.DeltaTime;
        var jumpInput = Input.GetMouseButton(0);//HACK:Main thread
        Entities.ForEach((ref Translation translation, ref JumpData jumpData, in GroundData groundData) => 
        {
            if (groundData.IsOnGround)
                jumpData.IsJumping = jumpInput;

            var dirSpeed = jumpData.IsJumping ? 1f : -1f;
            translation.Value.y += dirSpeed * deltaTime * (jumpData.JumpSpeed - (-2f -translation.Value.x));//TODO da rivedere con i NativeArray per la posizione

            if (translation.Value.y > 10)
                jumpData.IsJumping = false;


            if (translation.Value.y < 0)
                translation.Value.y = 0;

        }).Schedule();
    }
}
