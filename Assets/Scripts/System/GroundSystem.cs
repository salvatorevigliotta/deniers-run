﻿using Unity.Entities;
using Unity.Jobs;
using Unity.Transforms;

public class GroundSystem : SystemBase
{
    protected override void OnUpdate()
    {
        Entities.ForEach((ref GroundData groundData,in Translation translation) => 
        {
            groundData.IsOnGround = translation.Value.y <= 0.01f;
        }).Schedule();
    }
}
