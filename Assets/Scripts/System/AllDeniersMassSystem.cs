﻿using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;

public class AllDeniersMassSystem : SystemBase
{
    protected override void OnUpdate()
    {
        var allDeniersMass = GetEntityQuery(ComponentType.ReadOnly<DenierTag>(),
                                            ComponentType.ReadOnly<MassData>()).
                                            ToComponentDataArray<MassData>(Allocator.Temp);
        var allMass = 0f;
        foreach (var mass in allDeniersMass)
            allMass += mass.Value;


        Entities.
            WithAll<DenierTag,AllDeniesMassData>().
            ForEach((ref AllDeniesMassData allDeniesMassData) => 
            {
                allDeniesMassData.Value = allMass;
            }).Schedule();
    }
}
