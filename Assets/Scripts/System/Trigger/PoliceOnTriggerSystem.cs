﻿using Unity.Entities;
using Unity.Physics;
using Unity.Physics.Systems;

public class PoliceOnTriggerSystem : SystemBase
{
    private BuildPhysicsWorld buildPhysicsWorld;
    private StepPhysicsWorld stepPhysicsWorld;

    private EndSimulationEntityCommandBufferSystem endFixedStepSimulationECBSystem; //TODO rivedere il nome

    protected override void OnCreate()
    {
        base.OnCreate();

        buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
        stepPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();
        endFixedStepSimulationECBSystem =
            World.GetOrCreateSystem<EndSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        var newX = UnityEngine.Random.Range(-6f, -2f);
        var newZ = UnityEngine.Random.Range(-1f, 1f);

        Dependency = new PoliceOnTriggerSystemJob
        {
            AllDeniers = GetComponentDataFromEntity<DenierTag>(true),
            DeniersMass = GetComponentDataFromEntity<AllDeniesMassData>(true),
            AllPolice = GetComponentDataFromEntity<PoliceTag>(true),
            PoliceMass = GetComponentDataFromEntity<MassData>(true),
            RandomX = newX,
            RandomZ = newZ,
            EntityCommandBuffer = endFixedStepSimulationECBSystem.CreateCommandBuffer(),
        }.Schedule(stepPhysicsWorld.Simulation, ref buildPhysicsWorld.PhysicsWorld, Dependency);

        endFixedStepSimulationECBSystem.AddJobHandleForProducer(Dependency);
    }
}