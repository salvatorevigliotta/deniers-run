﻿using Unity.Entities;
using Unity.Physics;
using Unity.Physics.Systems;
using Unity.Rendering;

public class PeopleOnTriggerSystem : SystemBase
{
    private BuildPhysicsWorld buildPhysicsWorld;
    private StepPhysicsWorld stepPhysicsWorld;

    private EndFixedStepSimulationEntityCommandBufferSystem endFixedStepSimulationECBSystem; //TODO rivedere il nome

    protected override void OnCreate()
    {
        base.OnCreate();

        buildPhysicsWorld = World.GetOrCreateSystem<BuildPhysicsWorld>();
        stepPhysicsWorld = World.GetOrCreateSystem<StepPhysicsWorld>();
        endFixedStepSimulationECBSystem = 
            World.GetOrCreateSystem<EndFixedStepSimulationEntityCommandBufferSystem>();
    }

    protected override void OnUpdate()
    {
        var newX = UnityEngine.Random.Range(-6f, -2f);
        var newZ = UnityEngine.Random.Range(-1f, 1f);
        Dependency = new PeopleOnTriggerSystemJob
        {
            AllDeniers = GetComponentDataFromEntity<DenierTag>(true),
            AllPeoples = GetComponentDataFromEntity<PeopleTag>(true),
            AllMass = GetComponentDataFromEntity<MassData>(true),
            RandomX = newX,
            RandomZ = newZ,
            EntityCommandBuffer = endFixedStepSimulationECBSystem.CreateCommandBuffer(),
        }.Schedule(stepPhysicsWorld.Simulation,
                   ref buildPhysicsWorld.PhysicsWorld,
                   Dependency);

        endFixedStepSimulationECBSystem.AddJobHandleForProducer(Dependency);
    }
}
