﻿using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Transforms;

public struct PoliceOnTriggerSystemJob : ITriggerEventsJob
{
    [ReadOnly] public ComponentDataFromEntity<DenierTag> AllDeniers;
    [ReadOnly] public ComponentDataFromEntity<AllDeniesMassData> DeniersMass;

    [ReadOnly] public ComponentDataFromEntity<PoliceTag> AllPolice;
    [ReadOnly] public ComponentDataFromEntity<MassData> PoliceMass;

    [ReadOnly] public float RandomX;
    [ReadOnly] public float RandomZ;

    public EntityCommandBuffer EntityCommandBuffer;

    public void Execute(TriggerEvent triggerEvent)
    {
        var denierEntity = AllDeniers.HasComponent(triggerEvent.EntityA) ? triggerEvent.EntityA :
                    AllDeniers.HasComponent(triggerEvent.EntityB) ? triggerEvent.EntityB :
                    Entity.Null;

        var policeEntity = AllPolice.HasComponent(triggerEvent.EntityA) ? triggerEvent.EntityA :
                    AllPolice.HasComponent(triggerEvent.EntityB) ? triggerEvent.EntityB :
                    Entity.Null;

        if (denierEntity == Entity.Null || policeEntity == Entity.Null)
            return;

        var policeMass = PoliceMass[policeEntity];
        var allDeniersMass = DeniersMass[denierEntity];

        if(policeMass.Value < allDeniersMass.Value)
        {
            var newPolice = EntityCommandBuffer.Instantiate(policeEntity);
            EntityCommandBuffer.SetComponent<Translation>(newPolice, new Translation()
            {
                Value = new float3(math.abs(RandomX)*20, 0, RandomZ),
            });//BUG : runtime memory leak

            var newDenier = EntityCommandBuffer.Instantiate(denierEntity);
            EntityCommandBuffer.SetComponent<Translation>(newDenier, new Translation()
            {
                Value = new float3(RandomX, 0, RandomZ),
            });

            EntityCommandBuffer.DestroyEntity(policeEntity);
        }
        else
        {
            EntityCommandBuffer.DestroyEntity(denierEntity);
        }
    }
}
