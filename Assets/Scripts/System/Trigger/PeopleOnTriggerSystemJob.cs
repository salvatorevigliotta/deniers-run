﻿using Unity.Collections;
using Unity.Entities;
using Unity.Mathematics;
using Unity.Physics;
using Unity.Transforms;

public struct PeopleOnTriggerSystemJob : ITriggerEventsJob
{
    [ReadOnly] public ComponentDataFromEntity<DenierTag> AllDeniers;
    [ReadOnly] public ComponentDataFromEntity<PeopleTag> AllPeoples;

    [ReadOnly] public ComponentDataFromEntity<MassData> AllMass;
    
    [ReadOnly] public float RandomX;
    [ReadOnly] public float RandomZ;

    public EntityCommandBuffer EntityCommandBuffer;

    public void Execute(TriggerEvent triggerEvent)
    {
        var denierEntity = AllDeniers.HasComponent(triggerEvent.EntityA) ? triggerEvent.EntityA : 
                            AllDeniers.HasComponent(triggerEvent.EntityB) ? triggerEvent.EntityB : 
                            Entity.Null;

        var peopleEntity = AllPeoples.HasComponent(triggerEvent.EntityA) ? triggerEvent.EntityA :
                    AllPeoples.HasComponent(triggerEvent.EntityB) ? triggerEvent.EntityB :
                    Entity.Null;

        if (denierEntity == Entity.Null || peopleEntity == Entity.Null)
            return;

        var denierMass = AllMass[denierEntity];
        var peopleMass = AllMass[peopleEntity];
        if(denierMass.Value < peopleMass.Value)
        {
            var newPeople = EntityCommandBuffer.Instantiate(peopleEntity);
            EntityCommandBuffer.SetComponent<Translation>(newPeople, new Translation()
            {
                Value = new float3(math.abs(RandomX)*15, 0, RandomZ),
            });//BUG: runtime memory leak

            EntityCommandBuffer.DestroyEntity(denierEntity);
        }
        else
        {
            EntityCommandBuffer.DestroyEntity(peopleEntity);

            var newDenier = EntityCommandBuffer.Instantiate(denierEntity);
            EntityCommandBuffer.SetComponent<Translation>(newDenier, new Translation()
            {
                Value = new float3(RandomX,0,RandomZ),
            });
        }
    }
}