# README #

Simple “endless runner” project with DOTS for Unity 2020.1.14f1

### Deniers Run!!! ###

The idea of the game comes from the evolution of the deniers who, until a few years ago, were simple generic conspirators. While now they have transformed themselves into a real danger by denying the global emergency to the point of creating real swarms of people ready to convert anyone who sets out on their path. Will the police be able to stop them?

### known issues ###

* Memory leak when respawn Entity after collision

### TODO ##
* Improve the deniers movement